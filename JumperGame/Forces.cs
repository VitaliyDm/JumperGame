﻿using System;
using System.Drawing;
using System.Linq;

namespace JumperGame
{
    public class Forces
    {
        public static PlayerForce GetThrustForce(Vector forceValue)
        {
            return r => forceValue;
        }

        public static PlayerForce ConvertGravityToForce(Gravity gravity, Size spaceSize)
        {
            return r => gravity(spaceSize, r.Location);
        }
        
        public static PlayerForce Sum(PlayerForce[] blackHoleForces, PlayerForce[] whiteHolesForces, PlayerForce gravity)
        {
            return r =>
            {
                var result = Vector.Zero;
                foreach (var force in blackHoleForces)
                    result = result + force(r);
                foreach (var force in whiteHolesForces)
                    result = result + force(r);
                result = result + gravity(r);
                return result;
            };
        }
    }
}