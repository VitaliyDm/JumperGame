﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace JumperGame
{
    public partial class GameForm : Form
    {
        private static Point mouseStartPoint;
        private static Point mouseFinishPoint;
        private static Point currentMousePoint;

        private bool endOfMapNear;
        private bool onSecretMap;

        private bool playerTurnLeft = true;
        private readonly Image player;
        private readonly Image playerJump;
        private readonly Image playerFall;
        private readonly Image platformJump;
        private readonly Image platformMagnet;
        private readonly Image platformSimple;
        private readonly Image platformBroke;
        private readonly Image blackHole;
        private readonly Image whiteHole;
        private readonly Image image;
        private readonly Image secretSpace;
        private readonly Image secretPlayer;
        private readonly Image portalToSecret;
        private readonly Image secretMap;
        private readonly Image defaultScene;

        private TextBox scoreText;

        private Queue<Image> mapImages;

        private readonly Timer timer;
        private GameScene gameScene;

        public static readonly Size windowSize = new Size(
            SystemInformation.PrimaryMonitorMaximizedWindowSize.Height / 16 * 10,
            SystemInformation.PrimaryMonitorMaximizedWindowSize.Height);

        public static Size SecretSpaceSize { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            DoubleBuffered = true;
            WindowState = FormWindowState.Normal;
        }
        public GameForm()
        {
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            InitializeComponent();
            
            player = Image.FromFile("GameObjects/Player.png");
            playerJump = Image.FromFile("GameObjects/Player_jump.png");
            playerFall = Image.FromFile("GameObjects/Player_fall.png");
            platformJump = Image.FromFile("GameObjects/JumpPlatform.png");
            platformMagnet = Image.FromFile("GameObjects/platform_1.png");
            platformSimple = Image.FromFile("GameObjects/platform_2.png");
            platformBroke = Image.FromFile("GameObjects/platform_3.png");
            blackHole = Image.FromFile("GameObjects/BlackHole.png");
            whiteHole = Image.FromFile("GameObjects/WhiteHole.png");
            secretPlayer = Image.FromFile("GameObjects/new_player.png");
            secretSpace = Image.FromFile("GameObjects/secret_space.png");
            portalToSecret = Image.FromFile("GameObjects/portal.png");

            defaultScene = new Bitmap(windowSize.Width, windowSize.Height * 2, PixelFormat.Format32bppArgb);

            onSecretMap = false;
            SecretSpaceSize = secretSpace.Size;

            image = new Bitmap(windowSize.Width, windowSize.Height, PixelFormat.Format32bppArgb);
            secretMap = new Bitmap(SecretSpaceSize.Width, SecretSpaceSize.Height, PixelFormat.Format32bppArgb);

            mapImages = new Queue<Image>();
            
            scoreText = new TextBox
            {
                Location = new Point(0, 0)
            };

            RestartGame();
            
            timer = new Timer { Interval = 3 };
            timer.Tick += TimerTick;
            timer.Start();

            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
            Size = new Size(windowSize.Width, windowSize.Height);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // GameForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Name = "GameForm";
            this.ResumeLayout(false);

        }

        private void RestartGame()
        {
            gameScene = new GameScene(
                (size, v) => new Vector(0, 10),
                windowSize.Width, windowSize.Height - 30);
            endOfMapNear = true;
            mapImages.Enqueue(defaultScene);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left && gameScene.SecretMapOn)
            {
                mouseStartPoint = e.Location;
                mouseFinishPoint = new Point(e.Location.X > windowSize.Width / 2? e.Location.X - 10: e.Location.X + 10, e.Location.Y + 10);
            } 
            if (e.Button == MouseButtons.Left &&
                gameScene.Player.Velocity.Y == 0 && gameScene.Player.Velocity.X == 0)
                mouseStartPoint = e.Location;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (mouseStartPoint != default(Point))
                currentMousePoint = e.Location;                
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left && 
                gameScene.Player.Velocity.Y == 0 && gameScene.Player.Velocity.X == 0)
                mouseFinishPoint = e.Location;
        }

        private void TimerTick(object sender, EventArgs e)
        {
            if (mouseFinishPoint != default(Point) || 
                (gameScene.Player.Velocity.X != 0 || gameScene.Player.Velocity.Y != 0))
            {
                MovePlayer();
                mouseStartPoint = default(Point);
                mouseFinishPoint = default(Point);
            }
           
            Invalidate();
            Update();
        }

        private void MovePlayer()
        {
            var speed = new Vector(
                mouseStartPoint.X - mouseFinishPoint.X,
                mouseStartPoint.Y - mouseFinishPoint.Y).Normalize();
            gameScene.MovePlayer(ClientRectangle.Size, speed * 15);

            if (gameScene.Player.Location.Y + gameScene.Player.Size.Height * 0.8 > windowSize.Height && !gameScene.SecretMapOn)
            {
                while (mapImages.Count != 0)
                {
                    mapImages.Dequeue();
                    gameScene.SceneMaps.Dequeue();
                }
                RestartGame();
            }
        }

        private void DrawDirection (Graphics g)
        {
            var from = new Point(
                    (int)(gameScene.Player.Location.X + gameScene.Player.Size.Width / 2),
                    (int)(gameScene.Player.Location.Y + gameScene.Player.Size.Height / 2));
            var directionAngle = new Vector(mouseStartPoint.X - currentMousePoint.X,
                 mouseStartPoint.Y - currentMousePoint.Y).Angle;
            var lineLength = windowSize.Width / 8;

            Pen pen = new Pen(Color.WhiteSmoke, 4);
            pen.EndCap = LineCap.ArrowAnchor;
            g.DrawLine(pen, from, 
                new Point(from.X + (int)(lineLength * Math.Cos(directionAngle)), from.Y + (int)(lineLength * Math.Sin(directionAngle))));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var g = Graphics.FromImage(image);
            //gameScene.MapChanged = false;
            if (gameScene.MapChanged)
            {
                if (gameScene.SecretMapOn)
                {
                    var map = Graphics.FromImage(secretMap);
                    DrawSecretMap(map, gameScene.SceneMaps.Peek());
                }
                else
                {
                    if (gameScene.BackToSimple)
                    {
                        mapImages.Dequeue();
                        mapImages.Enqueue(defaultScene);
                    }
                    var map = Graphics.FromImage(mapImages.Peek());
                    DrawCurrentMap(map, gameScene.SceneMaps.Peek());
                    endOfMapNear = false;
                }
                gameScene.MapChanged = false;
            }

            if (gameScene.EndOfMapNear)
            {
                var mapImage = new Bitmap(windowSize.Width, windowSize.Height * 2, PixelFormat.Format32bppArgb);
                var map = Graphics.FromImage(mapImage);
                DrawCurrentMap(map, gameScene.SceneMaps.Last());
                mapImages.Enqueue(mapImage);
                gameScene.EndOfMapNear = false;
            }

            scoreText.Text = gameScene.Score.ToString();
            this.Controls.Add(scoreText);

            DrawTo(g);
            e.Graphics.DrawImage(image, 0, 0);
        }

        private void DrawSecretMap(Graphics g, SceneMap elementsToDraw)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawImage(secretSpace, new Point(0, 0));
            foreach (var element in elementsToDraw.mapElements)
                g.DrawImage(portalToSecret,
                       (float)element.Location.X, (float)element.Location.Y
                           - elementsToDraw.MapOffset,
                       element.Size.Width, element.Size.Height);
        }

        private void DrawCurrentMap(Graphics g, SceneMap elementsToDraw)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.FillRectangle(Brushes.Navy, 0, 0, windowSize.Width, windowSize.Height * 2);

            var matrix = g.Transform;

            foreach (var element in elementsToDraw.mapElements)
            {
                if (element.Visibility)
                {
                    Image elementImage;
                    switch (element.Type)
                    {
                        case ObjectType.platformJump:
                            elementImage = platformJump;
                            break;
                        case ObjectType.platformBroke:
                            elementImage = platformBroke;
                            break;
                        case ObjectType.platformMagnet:
                            elementImage = platformMagnet;
                            break;
                        case ObjectType.platformSimple:
                            elementImage = platformSimple;
                            break;
                        case ObjectType.blackHole:
                            elementImage = blackHole;
                            break;
                        case ObjectType.whiteHole:
                            elementImage = whiteHole;
                            break;
                        case ObjectType.portal:
                            elementImage = portalToSecret;
                            break;
                        default:
                            throw new FormatException("Used unknown Game Object");
                    }
                    g.DrawImage(elementImage,
                        (float)element.Location.X, (float)element.Location.Y 
                            - elementsToDraw.MapOffset,
                        element.Size.Width, element.Size.Height);
                }
            }
         }

        private void DrawTo(Graphics g)
        {
            if (gameScene.SecretMapOn)
            {
                while (mapImages.Count > 1)
                    mapImages.Dequeue();
                g.DrawImage(secretMap, gameScene.SceneMaps.Last().CamepaPoint);
                g.DrawImage(secretPlayer, (float)gameScene.Player.Location.X, (float)gameScene.Player.Location.Y,
                    gameScene.Player.Size.Width, gameScene.Player.Size.Height);
                return;
            }

            if (mapImages.Count > 1)
            {
                if (gameScene.SceneMaps.Count < 2)
                    mapImages.Dequeue();
                else
                    g.DrawImage(mapImages.Last(), new Point(0, gameScene.SceneMaps.Last().MapOffset));
            }

            g.DrawImage(mapImages.Peek(), new Point(0, gameScene.SceneMaps.Peek().MapOffset));
            var matrix = g.Transform;

            if (mouseStartPoint != default(Point))
            {
                DrawDirection(Graphics.FromImage(image));
                //drawingDirection = false;
            }

            if (gameScene.Player.Velocity.Y < 0)
            {
                if (gameScene.Player.Velocity.X > 0 && playerTurnLeft)
                {
                    playerJump.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    playerTurnLeft = false;
                }
                else if (gameScene.Player.Velocity.X < 0 && !playerTurnLeft)
                {
                    playerJump.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    playerTurnLeft = true;
                }
                g.DrawImage(playerJump, (float)gameScene.Player.Location.X, (float)gameScene.Player.Location.Y,
                    gameScene.Player.Size.Width, gameScene.Player.Size.Height);
            }
            else if (gameScene.Player.Velocity.Y > 0)
                g.DrawImage(playerFall, (float)gameScene.Player.Location.X, (float)gameScene.Player.Location.Y,
                    gameScene.Player.Size.Width, gameScene.Player.Size.Height);
            else
                g.DrawImage(player, (float)gameScene.Player.Location.X, (float)gameScene.Player.Location.Y,
                    gameScene.Player.Size.Width, gameScene.Player.Size.Height);

        }
    }
}
