﻿using System.Drawing;

namespace JumperGame
{
    public class GameObject
    {
        public Vector Location { get; set; }
        public readonly ObjectType Type;
        public bool Visibility { get; set; }
        public readonly Size Size;

        public Rectangle Colider { get; set; }

        public GameObject(Vector location, ObjectType type, bool visibility, Size objectSize)
        {
            Location = location;
            Type = type;
            Visibility = visibility;
            Size = objectSize;
            Colider = new Rectangle((int)location.X, (int)location.Y, objectSize.Width * 9 / 10, 
                type == ObjectType.platformJump || type == ObjectType.platformSimple? 
                (int)(objectSize.Height * 0.6): (int)(objectSize.Height * 0.8));
        }

        public void MoveDown(double offset)
        {
            Location = new Vector(Location.X, Location.Y + offset);
            Colider = new Rectangle((int)Location.X, (int)Location.Y, Size.Width * 9 / 10,
                Type == ObjectType.platformJump || Type == ObjectType.platformSimple ?
                (int)(Size.Height * 0.6) : (int)(Size.Height * 0.8));
        }
    }
}
