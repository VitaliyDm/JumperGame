﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;

namespace JumperGame
{
    class GameScene
    {
        public static Size PlatformSize { get; private set; }
        public static Size JumpPlatformSize { get; private set; }
        public static Size PlayerSize { get; private set; }
        public static Size PlayerJumpSize { get; private set; }
        public static Size HoleSize { get; private set; }

        public int Score { get; private set; }

        public Queue<SceneMap> SceneMaps;

        private readonly Size mapSize;
        private ObjectType platfromUnderPlayer;

        //Show gap between element on screen in pixels
        private static readonly int gap = 10;
        private const double dt = 0.3;

        public Player Player;
        public bool SecretMapOn;
        public bool BackToSimple;

        private Queue<GameObject> elementsOnScene;

        public Gravity Gravity { get; private set; }
        public bool MapChanged;
        public bool EndOfMapNear;
        public int MapOffset;
        private int iterCount;

        public GameScene(Gravity gravity, int mapWidth, int mapHeight)
        {
            Gravity = gravity;

            SecretMapOn = false;
            BackToSimple = false;
            Score = 0;

            mapSize = new Size(mapWidth, mapHeight);
            SceneMaps = new Queue<SceneMap>();
            elementsOnScene = new Queue<GameObject>();

            PlatformSize = new Size((int)(0.13 * mapSize.Height), (int)(0.047 * mapSize.Height));
            JumpPlatformSize = new Size((int)(0.13 * mapSize.Height), (int)(0.06 * mapSize.Height));
            PlayerSize = new Size((int)(0.0672 * mapSize.Height), (int)(0.1 * mapSize.Height));
            PlayerJumpSize = new Size((int)(0.0495 * mapSize.Height), (int)(0.1 * mapSize.Height));
            HoleSize = new Size((int)(0.08 * mapSize.Height), (int)(0.13 * mapSize.Height));

            SceneMaps.Enqueue(new SceneMap(true, true, SecretMapOn));
            Player = SceneMaps.Peek().Player;
            foreach (var element in SceneMaps.Peek().mapElements)
                elementsOnScene.Enqueue(element);
            platfromUnderPlayer = SceneMaps.Peek().mapElements.Peek().Type;
            MapOffset = SceneMaps.Peek().MapOffset;
            MapChanged = true;
        }

        public void MovePlayer(Size sceneSize, Vector speed)
        {
            iterCount++;

            var blackHolesForces = SceneMaps.Peek().blackHoles
                .Select(x => Forces.GetThrustForce(GetGravityBlackHole(Player.Location, x.Location))).ToArray();

            var whiteHolesForces = SceneMaps.Peek().whiteHoles
                .Select(x => Forces.GetThrustForce(GetGravityWhiteHole(Player.Location, x.Location))).ToArray();

            var force = Forces.Sum(blackHolesForces, whiteHolesForces, Forces.ConvertGravityToForce(Gravity, sceneSize));

            var dir = Player.Direction;
            var velocity = (new Vector(
                Player.Velocity.X + speed.X,
                Player.Velocity.Y + speed.Y + force(Player).Y * dt * dt / 2) + GetStartBonuse());
            var location = (Player.Location + velocity * dt);

            velocity = ChangeVelocity(location, velocity);
            
            Player = new Player(location.BoundTo(sceneSize), velocity, dir, 
                velocity.Y >= 0? PlayerSize: PlayerJumpSize);
        }

        private Vector GetStartBonuse()
        {
            switch (platfromUnderPlayer)
            {
                case ObjectType.platformJump:
                    return new Vector(0, -0.1);
                case ObjectType.platformMagnet:
                    return new Vector(0, 0.1);
            }

            return new Vector(0, 0);
        }

        private Vector ChangeVelocity(Vector nextLocation, Vector velocity)
        {
            ///<summary>
            /// This method change speed of player, also it change spee of Main game camera
            ///</summary>

            if (!SecretMapOn)
            {
                if (nextLocation.X < 0 && velocity.X < 0)
                    velocity = new Vector(-velocity.X, velocity.Y) * 0.7;
                if (nextLocation.X > mapSize.Width - PlayerSize.Width && velocity.X > 0)
                    velocity = new Vector(-velocity.X, velocity.Y) * 0.7;
            }
            else
            {
                if (nextLocation.Y > GameForm.windowSize.Height - Player.Size.Height*1.5 && velocity.Y > 0)
                    velocity = new Vector(velocity.X, 0);
                if ((nextLocation.X > GameForm.SecretSpaceSize.Width - GameForm.windowSize.Width && velocity.X > 0) ||
                    (nextLocation.X < 0 && velocity.X < 0))
                    return new Vector(0, velocity.Y);
                if (iterCount > 300)
                    BackToGame();
            }
            var mapOffset = (Player.Location.Y - nextLocation.Y)*2;
            var mapOffsetDelta = mapOffset > 0 ? (int)mapOffset + 1 : 0;
            var offsetX = Player.Location.X - nextLocation.X;

            ////Debuging method
            //mapOffsetDelta = 10;
            //velocity = new Vector(0, -100);

            if (!SecretMapOn)
                MoveCamera(mapOffsetDelta);
            else
            {
                var currentPoint = SceneMaps.Last().CamepaPoint;
                SceneMaps.Last().CamepaPoint =
                    new Point(currentPoint.X + offsetX < 0
                        && currentPoint.X + offsetX > GameForm.windowSize.Width - GameForm.SecretSpaceSize.Width?
                       (int)(currentPoint.X + offsetX): currentPoint.X,
                    currentPoint.Y + mapOffset < 0
                        && currentPoint.Y + mapOffset > GameForm.windowSize.Height - GameForm.SecretSpaceSize.Height?
                        (int)(currentPoint.Y + mapOffset): currentPoint.Y);
            }

            var currentPlayerLocation = new Vector(Player.Location.X, Player.Location.Y + mapOffsetDelta);
            var nextPlayerLocation = new Vector(nextLocation.X, nextLocation.Y + mapOffsetDelta);


            foreach (var obj in elementsOnScene)
            {
                //Checking current speed and move Camera by moving all ogjects on map
                if (nextLocation.Y < Player.Location.Y)
                {
                    obj.MoveDown(mapOffsetDelta);
                    if (obj.Location.Y > mapSize.Height) obj.Visibility = false;
                }

                if (obj.Location.Y < 0)
                    continue;

                ////Отладочный метод
                //if (nextLocation.Y > Player.Location.Y)
                //    return new Vector(0, 0);

                //this coef is using because jumpPlatform and simplePlatform have decoration at the top
                var coefficent = (obj.Type == ObjectType.platformJump ? 0.75: obj.Type == ObjectType.platformSimple? 0.85: 1);
                var playerColider = new Rectangle(
                    new Point((int)nextLocation.X, (int)(nextLocation.Y + Player.Size.Height * coefficent)),
                    new Size((int)(Player.Size.Width * 0.4), (int)(Player.Size.Height / 32)));

                if (obj.Type == ObjectType.portal && obj.Colider.IntersectsWith(playerColider))
                {
                    if (!SecretMapOn)
                        CreateSecretMap();
                    else
                        BackToGame();

                    return velocity;
                }

                if (obj.Visibility && !SecretMapOn && obj.Type != ObjectType.blackHole && obj.Type != ObjectType.whiteHole
                    && nextLocation.Y > Player.Location.Y
                    && obj.Colider.IntersectsWith(playerColider)
                   )
                {
                    if (obj.Type != ObjectType.platformBroke)
                    {
                        nextLocation = Player.Location;
                        platfromUnderPlayer = obj.Type;
                        velocity = new Vector(0, 0);
                    }
                    else
                    {
                        obj.Visibility = false;
                        MapChanged = true;
                    }
                }
            }

            ClearUnvisableObjects();
            return velocity;
        }

        private void BackToGame()
        {   
            ClearScene();
            SceneMaps.Dequeue();
            SecretMapOn = false;
            SceneMaps.Enqueue(new SceneMap(true, false, SecretMapOn));
            AddElementsOnScene();
            BackToSimple = true;
            MapChanged = true;
            iterCount = 0;
        }

        private void MoveCamera(int offset)
        {
            Score += offset/2;
            foreach (var map in SceneMaps)
            {
                map.MapOffset += offset;

                if (SceneMaps.Peek().MapOffset > 0 && !SceneMaps.Peek().AddedNextMap)
                {
                    SceneMaps.Enqueue(new SceneMap(false, false, SecretMapOn));
                    SceneMaps.Peek().AddedNextMap = true;
                    AddElementsOnScene();
                    EndOfMapNear = true;
                    break;
                }
                if (SceneMaps.Peek().MapOffset > mapSize.Height)
                {
                    SceneMaps.Dequeue();
                    break;
                }
            }
        }

        private void AddElementsOnScene()
        {
            foreach (var element in SceneMaps.Last().mapElements)
            {
                if (!SecretMapOn)
                    element.Location = new Vector(element.Location.X, element.Location.Y - mapSize.Height);
                elementsOnScene.Enqueue(element);
            }
        }

        private void ClearScene()
        {
            while (elementsOnScene.Count > 0)
                elementsOnScene.Dequeue();
        }

        private void ClearUnvisableObjects()
        {
            if (SecretMapOn) return;
            while (elementsOnScene.Peek().Location.Y > GameForm.windowSize.Height)
                elementsOnScene.Dequeue();
        }

        private Vector GetGravityBlackHole(Vector location, Vector blackHoleLocation)
        {
            var deltaBlack = blackHoleLocation - location;
            var coeffBlack = 800 / (deltaBlack.Length * deltaBlack.Length + 1);
            return deltaBlack * coeffBlack;
        }

        private Vector GetGravityWhiteHole(Vector location, Vector whiteHoleLocation)
        {
            var deltaWhite = location - whiteHoleLocation;
            var coeffWhite = 600 / (deltaWhite.Length * deltaWhite.Length + 1);
            return deltaWhite * coeffWhite;
        }

        private void CreateSecretMap()
        {
            SecretMapOn = true;
            while (SceneMaps.Count > 0)
                SceneMaps.Dequeue();
            SceneMaps.Enqueue(new SceneMap(false, false, SecretMapOn)
            { CamepaPoint = new Point(
                GameForm.windowSize.Width - GameForm.SecretSpaceSize.Width, GameForm.windowSize.Height - GameForm.SecretSpaceSize.Height) });
            MapChanged = true;
            ClearScene();
            AddElementsOnScene();
        }
    }
}
