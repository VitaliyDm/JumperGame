﻿using System.Drawing;

namespace JumperGame
{
    public delegate Vector Gravity(Size windowSize, Vector location);
}