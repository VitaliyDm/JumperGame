﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumperGame
{
    public enum ObjectType
    {
        platformJump= 0,
        platformBroke = 1,
        platformMagnet = 2,
        platformSimple = 3,
        blackHole = 4,
        whiteHole = 5,
        portal = 6
    }
}
