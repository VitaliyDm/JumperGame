﻿using System.Drawing;

namespace JumperGame
{
    public class Player
    {
        public enum PlayerState
        {
            OnPlatform = 0,
            Jumping = 1,
            Faling = 2
        }

        public readonly Vector Location;
        public readonly Vector Velocity;
        public readonly PlayerState Statement;
        public readonly double Direction;
        public readonly Size Size;
        public Rectangle Colider { get; set; }
        public readonly Size ColiderSize;

        public Player(Vector location, Vector velocity, double direction, Size size)
        {
            Location = location;
            Velocity = velocity;
            Direction = direction;
            Size = size;
            Colider = new Rectangle((int)location.X, (int)location.Y, size.Width * 8 / 10, size.Height * 9 / 10);
            Statement = velocity.Y > 0 ? velocity.Y < 0 ? PlayerState.Faling : PlayerState.OnPlatform : PlayerState.Jumping;
        }
    }
}
