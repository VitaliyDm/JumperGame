﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumperGame
{
    class SceneMap
    {
        public System.Drawing.Point CamepaPoint { get; set; }
        public int MapOffset { get; set; }
        public bool MapChanged;
        public Queue<GameObject> mapElements { get; private set; }
        public Queue<GameObject> blackHoles { get; private set; }
        public Queue<GameObject> whiteHoles { get; private set; }
        public Player Player;
        public bool AddedNextMap { get; set; }

        private Tuple<int, int> jumpElipse;
        private ObjectElipse simpleElipse;
        private ObjectElipse magnetElipse;

        private const int gap = 10;

        private Random rnd = new Random();

        public SceneMap(bool startMap, bool initPlayer,  bool secretMap)
        {
            AddedNextMap = false;
            MapChanged = true;
            MapOffset = -GameForm.windowSize.Height * 2 + gap;

            mapElements = new Queue<GameObject>();
            blackHoles = new Queue<GameObject>();
            whiteHoles = new Queue<GameObject>();

            jumpElipse = new Tuple<int, int>((int)(GameScene.PlatformSize.Width*1.2), (int)GameScene.PlatformSize.Width * 2);


            if (!secretMap)
            {
                if (startMap)
                {
                    MapOffset = -GameForm.windowSize.Height - GameScene.PlatformSize.Height;
                    InitializeStartPosition(initPlayer);
                }

                GenerateObjects();
            }
            else
                GereateSecretMap();
                
        }

        private void InitializeStartPosition(bool initPlayer)
        {
            var random = new Random();

            var startPosition = new Vector(
               random.Next(GameScene.JumpPlatformSize.Width / 2 + gap, GameForm.windowSize.Width - GameScene.JumpPlatformSize.Width / 2 - 2 * gap),
               GameForm.windowSize.Height - GameScene.JumpPlatformSize.Height - gap - random.Next(0, (int)(GameForm.windowSize.Height * 0.15)));
            
            //Initialize PLayer
            if (initPlayer)
              Player = new Player(
                    new Vector(startPosition.X, (int)(startPosition.Y - GameScene.PlayerSize.Height * 0.75)),
                    new Vector(0, 0), 0, GameScene.PlayerSize);

            //Initilize start platform
            mapElements.Enqueue(new GameObject(startPosition, ObjectType.platformJump, true, GameScene.JumpPlatformSize));
            //platfromUnderPlayer = ObjectType.platformJump;
        }
        private void GenerateObjects()
        {
            var rnd = new Random();
            var lastItemLocation = new Vector(GameForm.windowSize.Width, GameForm.windowSize.Height);
            ObjectType lastItemType = ObjectType.platformSimple;
            if (mapElements.Count != 0)
            {
                var lastItem = mapElements.Last();
                lastItemLocation = lastItem.Location;
                lastItemType = lastItem.Type;
            }
            while (lastItemLocation.Y > -GameForm.windowSize.Height)
                GeneratePlatform(ref lastItemLocation, ref lastItemType);

            for (var i = 0; i < rnd.Next(1, 3); i++)
            {
                var blackHole = new GameObject(
                    new Vector(rnd.Next(GameScene.HoleSize.Width / 2, GameForm.windowSize.Width - GameScene.HoleSize.Width / 2),
                    rnd.Next(- GameScene.HoleSize.Height / 2, GameForm.windowSize.Height - GameScene.HoleSize.Height / 2)),
                    ObjectType.blackHole, true, GameScene.HoleSize);
                mapElements.Enqueue(blackHole);
                blackHoles.Enqueue(blackHole);
            }
            for (var i = 0; i < rnd.Next(1, 3); i++)
            {
                var whiteHole = new GameObject(
                    new Vector(rnd.Next(GameScene.HoleSize.Width / 2, GameForm.windowSize.Width - GameScene.HoleSize.Width / 2),
                    rnd.Next(-GameScene.HoleSize.Height / 2, GameForm.windowSize.Height - GameScene.HoleSize.Height / 2)),
                    ObjectType.whiteHole, true, GameScene.HoleSize);
                mapElements.Enqueue(whiteHole);
                whiteHoles.Enqueue(whiteHole);
            }

            var portal = new GameObject(
                   new Vector(rnd.Next(GameScene.HoleSize.Width / 2, GameForm.windowSize.Width - GameScene.HoleSize.Width / 2),
                   rnd.Next(-GameScene.HoleSize.Height / 2, GameForm.windowSize.Height - GameScene.HoleSize.Height / 2)),
                   ObjectType.portal, true, GameScene.HoleSize);
            mapElements.Enqueue(portal);
        }

        private void GeneratePlatform(ref Vector lastItemLocation, ref ObjectType type)
        {
            var locationX = 0.0;
            var locationY = 0.0;
            var createBrokePlatform = false;

            var rndNum = rnd.Next(100);

            if (lastItemLocation.X > GameForm.windowSize.Width / 2 && rndNum < 60)
                locationX = rnd.Next(gap, GameForm.windowSize.Width / 2 - GameScene.PlatformSize.Width * 2);
            else
                locationX = rnd.Next(GameForm.windowSize.Width / 2, GameForm.windowSize.Width - GameScene.JumpPlatformSize.Width - 2 * gap);

            rndNum = rnd.Next(100);

            if (rndNum < 15 )
                locationY = lastItemLocation.Y - 1;
            else if (rndNum < 70 && (mapElements.Count!=0 && mapElements.Peek().Type == ObjectType.platformBroke))
                locationY = lastItemLocation.Y - GameScene.PlayerSize.Height * 0.4 - gap - rnd.Next((int)(GameForm.windowSize.Height * 0.12));
            else
                locationY = lastItemLocation.Y - GameScene.PlayerSize.Height * 0.8 - 3 * gap - rnd.Next((int)(GameForm.windowSize.Height * 0.12));


            if (lastItemLocation.Y - locationY < GameScene.PlayerSize.Height * 0.5)
                createBrokePlatform = true;

            lastItemLocation = new Vector(locationX, locationY);

            type = ObjectType.platformSimple;
            var size = GameScene.PlatformSize;
            rndNum = rnd.Next(4);
            if (rndNum==0)
                    type = ObjectType.platformSimple;
            else if (rndNum == 1)
                    type = ObjectType.platformMagnet;
            else if (rndNum == 2 && createBrokePlatform)
                    type = ObjectType.platformBroke;
            else
            {
                type = ObjectType.platformJump;
                size = GameScene.JumpPlatformSize;
            }

            if (lastItemLocation.Y > -GameForm.windowSize.Height)
                mapElements.Enqueue(new GameObject(lastItemLocation, type, true, size));
        }

        private void GereateSecretMap()
        {
            for (var i=0; i < rnd.Next(1, 3); i++)
                mapElements.Enqueue(new GameObject(new Vector(
                    rnd.Next(GameForm.windowSize.Width - 2 * GameScene.HoleSize.Width) + GameScene.HoleSize.Width
                    ,rnd.Next(GameForm.windowSize.Height - 2 * GameScene.HoleSize.Height) + GameScene.HoleSize.Height),
                    ObjectType.portal, true, GameScene.HoleSize));
        }

        private class ObjectElipse
        {
            public int A;
            public int B;

            public ObjectElipse(int a, int b)
            {
                A = a;
                B = b;
            }
        }
    }
}
