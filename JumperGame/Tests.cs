﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JumperGame
{
    [TestFixture]
    public class TestsMaps
    {
        private void TestMaps(bool startMap, bool initPlayer, bool secretMap)
        {
            var newMap = new SceneMap(startMap, initPlayer, secretMap);

            Assert.IsNotNull(newMap);

            if (initPlayer) Assert.IsNotNull(newMap.Player);
            else Assert.IsNull(newMap.Player);

            Assert.IsNotNull(newMap.mapElements);
            if (!secretMap)
                Assert.IsTrue(newMap.mapElements.Count > 3 && newMap.whiteHoles.Count > 0 && newMap.blackHoles.Count > 0);
            else
                Assert.IsTrue(newMap.mapElements.Count > 0);

            Assert.IsTrue(newMap.MapChanged);
            Assert.IsFalse(newMap.AddedNextMap);
        }

        [Test]
        public void TestStartMapCreation() { TestMaps(true, true, false); }

        [Test]
        public void TestSecretMapCreating() { TestMaps(false, false, true); }

        [Test]
        public void TestCreatingMapAfterSecret() { TestMaps(true, false, false); }

        [Test]
        public void TestCreatingSimpleMap() { TestMaps(false, false, false); }

    }

    [TestFixture]
    public class TestForces
    {
        private static Player player = new Player(new Vector(0, 0), new Vector(0, 0), 0, new System.Drawing.Size(100, 100));
        private void TestConvertation(int x, int y)
        {
            Assert.AreEqual(new Vector(x, y),
                Forces.GetThrustForce(new Vector(x, y))(player));
        }

        [Test]
        public void TestConvertation1() { TestConvertation(0, 0); }

        [Test]
        public void TestConvertation2() { TestConvertation(1, 2); }

        [Test]
        public void TestConvertation3() { TestConvertation(-1, -1); }

        [Test]
        public void TestConvertation4() { TestConvertation(1, -1); }

        [Test]
        public void TestConvertation5() { TestConvertation(-2, 2); }

        private void TestGravityCreation(double x, double y)
        {
            var spaceSize = new System.Drawing.Size(100, 100);
            Assert.AreEqual(new Vector(x, y), Forces.ConvertGravityToForce(new Gravity((size, v) => new Vector(x, y)), spaceSize)
                (player));
        } 

        [Test]
        public void TestGravity1() { TestGravityCreation(0, 0); }

        [Test]
        public void TestGravity2() { TestGravityCreation(10, 0); }

        [Test]
        public void TestGravity3() { TestGravityCreation(0, 10); }

        [Test]
        public void TestGravity4() { TestGravityCreation(-1, -1); }

        private void TestForcesSum(Vector result, PlayerForce[] blackHoleForces, PlayerForce[] whiteHolesForces, PlayerForce gravity)
        {
            Assert.AreEqual(result, Forces.Sum(blackHoleForces, whiteHolesForces, gravity)(player));
        }

        [Test]
        public void TestNoneForces() { TestForcesSum(Vector.Zero, new PlayerForce[0], new PlayerForce[0], 
            new PlayerForce(Forces.GetThrustForce(new Vector(0, 0)))); }

        private void CheckForces(Vector vector1, Vector vector2, Vector vector3)
        {
            var vectorSum = vector1 + vector2 + vector3;
            var force1 = Forces.GetThrustForce(vector1);
            var force2 = Forces.GetThrustForce(vector2);
            var force3 = Forces.GetThrustForce(vector3);
            TestForcesSum(vectorSum, new PlayerForce[] { force1 }, new PlayerForce[] { force2 }, force3);
            TestForcesSum(vectorSum, new PlayerForce[] { force1, force2, force3 }, new PlayerForce[0],
                new PlayerForce(Forces.GetThrustForce(new Vector(0, 0))));
            TestForcesSum(vectorSum, new PlayerForce[] { force3 }, new PlayerForce[] { force1, force2 },
                new PlayerForce(Forces.GetThrustForce(new Vector(0, 0))));
            TestForcesSum(vectorSum, new PlayerForce[0], new PlayerForce[] { force1, force2, force3 },
                new PlayerForce(Forces.GetThrustForce(new Vector(0, 0))));
        }

        [Test]
        public void TestSum1() { CheckForces(new Vector(12, 0), new Vector(-13, 0), new Vector(11, 0)); }

        [Test]
        public void TestSum2() { CheckForces(new Vector(0, 12), new Vector(0, -13), new Vector(0, 12)); }

        [Test]
        public void TestSum3() { CheckForces(new Vector(12, 12), new Vector(12, 12), new Vector(-12, -12)); }
    }

    [TestFixture]
    class TestingGameScene
    {
        [Test]
        public void GameSceneCreation()
        {
            var gameScene = new GameScene((size, v) => new Vector(0 , 10), 100, 100);
            Assert.IsNotNull(gameScene);
            Assert.True(gameScene.MapChanged);
            Assert.False(gameScene.SecretMapOn);
            Assert.False(gameScene.EndOfMapNear);
            Assert.IsNotNull(gameScene.Player);
            Assert.IsNotNull(gameScene.SceneMaps);
        }
        
        private void TestPLayerMovement(Vector playerSpeed)
        {
            var sceneWidth = 100;
            var sceneHeihght = 100;
            var gameScene = new GameScene((size, v) => new Vector(0, 10), sceneWidth, sceneHeihght);

            var currentCamera = gameScene.SceneMaps.Peek().MapOffset;
            var currentPlayerStatement = gameScene.Player;
            gameScene.MovePlayer(new System.Drawing.Size(sceneWidth, sceneHeihght), playerSpeed);
            Assert.AreNotEqual(currentPlayerStatement.Location, gameScene.Player.Location);
            Assert.AreNotEqual(currentPlayerStatement.Velocity, gameScene.Player.Velocity);
            if (playerSpeed.Y < 0)
            {
                Assert.AreNotEqual(currentCamera, gameScene.SceneMaps.Peek().MapOffset);
            }      
        }

        [Test]
        public void TestSpeedChanging1() { TestPLayerMovement(new Vector(0, 0)); }

        [Test]
        public void TestSpeedChanging2() { TestPLayerMovement(new Vector(120, 0)); }

        [Test]
        public void TestSpeedChanging3() { TestPLayerMovement(new Vector(-120, 0)); }

        [Test]
        public void TestSpeedChanging4() { TestPLayerMovement(new Vector(0, 120)); }

        [Test]
        public void TestSpeedChanging5() { TestPLayerMovement(new Vector(0, -120)); }

        [Test]
        public void TestSpeedChanging6() { TestPLayerMovement(new Vector(120, 120)); }

        [Test]
        public void TestSpeedChanging7() { TestPLayerMovement(new Vector(-120, 120)); }

        [Test]
        public void TestSpeedChanging8() { TestPLayerMovement(new Vector(10, -90)); }

        [Test]
        public void TestSpeedChanging9() { TestPLayerMovement(new Vector(-120, -120)); }
    }
}
